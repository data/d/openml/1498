# OpenML dataset: sa-heart

https://www.openml.org/d/1498

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: http://statweb.stanford.edu/~tibs/ElemStatLearn/data.html   
**Please cite**:   

* Title:

South Africa Heart Disease Dataset

* Description

A retrospective sample of males in a heart-disease high-risk region of the Western Cape, South Africa. There are roughly two controls per case of CHD. Many of the CHD positive men have undergone blood pressure reduction treatment and other programs to reduce their risk factors after their CHD event. In some cases the measurements were made after these treatments. These data are taken from a larger dataset, described in  Rousseauw et al, 1983, South African Medical
Journal. 

* Attributes:

sbp  systolic blood pressure   
tobacco  cumulative tobacco (kg)   
ldl  low densiity lipoprotein cholesterol   
adiposity  
famhist  family history of heart disease (Present, Absent)   
typea  type-A behavior   
obesity   
alcohol  current alcohol consumption   
age  age at onset   
chd  response, coronary heart disease

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1498) of an [OpenML dataset](https://www.openml.org/d/1498). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1498/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1498/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1498/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

